FROM mcr.microsoft.com/dotnet/aspnet:6.0

ARG VCS_REF
ARG VERSION
ARG BUILD_DATE

LABEL \
    org.label-schema.schema-version="1.0" \
    org.label-schema.name="tilt-dotnet-base" \
    org.label-schema.description="Image can be used as a base image to debug .NET applications with Tilt." \
    org.label-schema.vcs-url="https://gitlab.com/devgem/docker-tilt-dotnet" \
    org.label-schema.vendor="DevGem" \
    org.label-schema.vcs-ref=$VCS_REF \
    version=$VERSION \
    build-date=$BUILD_DATE

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get update && \
    apt-get -y install --no-install-recommends unzip curl && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    curl -sSL https://aka.ms/getvsdbgsh | /bin/sh /dev/stdin -v latest -l /vsdbg
