# Tilt Dotnet

[![tilt-dotnet-base](https://img.shields.io/badge/tilt--dotnet--base-0.3-brightgreen.svg)](https://hub.docker.com/r/devgem/tilt-dotnet--base/tags/)
 [![dotnet/aspnet](https://img.shields.io/badge/dotnet--aspnet-6.0-brightgreen.svg)](https://hub.docker.com/_/microsoft-dotnet-aspnet/)
 [![vsdbg](https://img.shields.io/badge/vsdbg-17.0.10712.2-brightgreen.svg)](https://github.com/Microsoft/MIEngine/wiki/Offroad-Debugging-of-.NET-Core-on-Linux---OSX-from-Visual-Studio)

[![Docker Stars](https://img.shields.io/docker/stars/devgem/tilt-dotnet-base.svg)](https://hub.docker.com/r/devgem/tilt-dotnet-base 'DockerHub')
 [![Docker Pulls](https://img.shields.io/docker/pulls/devgem/tilt-dotnet-base.svg)](https://hub.docker.com/r/devgem/tilt-dotnet-base 'DockerHub')
 [![Docker Layers](https://images.microbadger.com/badges/image/devgem/tilt-dotnet-base.svg)](https://microbadger.com/images/devgem/tilt-dotnet-base)
 [![Docker Version](https://images.microbadger.com/badges/version/devgem/tilt-dotnet-base.svg)](https://hub.docker.com/r/devgem/tilt-dotnet-base)

This docker image can be used as a base image to debug .NET applications with [Tilt](https://tilt.dev/).

To build the image:

```shell
export TILT_DOTNET_VERSION="0.3"
docker build -t devgem/tilt-dotnet-base:$TILT_DOTNET_VERSION --progress=plain --build-arg VERSION="$TILT_DOTNET_VERSION" --build-arg VCS_REF=`git rev-parse --short HEAD` --build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` .
```

## Usage

```Dockerfile
# use the image as a base image to enable debugging .NET when using Tilt
FROM devgem/tilt-dotnet-base:0.3
WORKDIR /src
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
COPY . .
ENTRYPOINT ["dotnet YOURPROJECT.dll"]
```

Add the `kube-debug.sh` file to the root-folder of your solution.

Add the following json to the `configurations` section of your `launch.json` file in VSCode:

```json
        {
            "name": "Attach to YOURPROJECT on Kubernetes",
            "type": "coreclr",
            "request": "attach", // we are going to attach to the existing pod
            "processName": "dotnet",
            "sourceFileMap": {
                // mapping of source code inside a container to the source code on a host machine
                "/src": "${workspaceRoot}/YOURPROJECT"
            },
            "pipeTransport": {
                "pipeProgram": "C:\\Program Files\\Git\\bin\\bash.exe",
                "pipeCwd": "${workspaceRoot}",
                "pipeArgs": [
                    "./kube-debug.sh",
                    "--selector app=YOURPROJECT",
                    "--namespace default"
                ],
                "quoteArgs": false,
                // path to installed debugger inside a container relative to WORKDIR
                "debuggerPath": "../vsdbg/vsdbg"
            }
        }
```

Replace `YOURPROJECT` by your stuff.

## Versions

Pre-built images are available on Docker Hub:

| image | date | dotnet/aspnet | vsdbg | remarks |
|-----|-----|-----|-----|-----|
| [`devgem/tilt-dotnet-base:0.3`](https://hub.docker.com/r/devgem/tilt-dotnet-base/tags/) | 20211227 | 6.0 | 17.0.10712.2 | |
| [`devgem/tilt-dotnet-base:0.2`](https://hub.docker.com/r/devgem/tilt-dotnet-base/tags/) | 20210520 | 5.0 | 16.9.20122.2 | |
| [`devgem/tilt-dotnet-base:0.1`](https://hub.docker.com/r/devgem/tilt-dotnet-base/tags/) | 20210118 | 5.0 | 16.9.20111.1 | |

## Updating on MicroBadger

```shell
curl -X POST https://hooks.microbadger.com/images/devgem/tilt-dotnet-base/rzrS6ErmTJlZ51ToNavOPXaJmWY=
```
